angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $ionicActionSheet, $timeout, DBService) {

        // Triggered on a button click, or some other target
        $scope.show = function() {

            // Show the action sheet
            var hideSheet = $ionicActionSheet.show({
                buttons: [
                    { text: '<b>Share</b> This' },
                    { text: 'Move' }
                ],
                destructiveText: 'Delete',
                titleText: 'Modify your album',
                cancelText: 'Cancel',
                cancel: function() {
                    // add cancel code..
                },
                buttonClicked: function(index) {
                    return true;
                }
            });
            // // For example's sake, hide the sheet after two seconds
            // $timeout(function() {
            //   hideSheet();
            // }, 2000);

        };

        var db = DBService.initDB();

        DBService.getInfo();
        //DBService.destroy();

        $scope.fileNameChanged = function(el) {
            console.log('file change');

            var file = el.files[0];

            db.put({
                _id: 'image',
                _attachments: {
                    filename: {
                        type: file.type,
                        data: file
                    }
                }
            }).catch(function(err) {
                console.log(err);
            });

        };

        db.get('image', { attachments: true }).then(function(doc) {
            console.log(doc._attachments.filename.data);
        });



        $scope.download = {
            url: '',
            bytesTotal: '',
            bytesReceived: '',
            fileEntry: null
        };

        $scope.submitDownload = function() {

            // if(typeof FileTransfer !== 'undefined') {
            //   var fileTransfer = new FileTransfer();
            //   var uri = encodeURI("http://media.ch9.ms/ch9/8c03/f4fe2512-59e5-4a07-bded-124b06ac8c03/PointerEventsCordovaPlugin.wmv");
            //   var filename = uri.split("/").pop();
            //   var target = cordova.file.applicationStorageDirectory + filename;
            //    // https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/index.html#file-system-layouts

            //   console.log(target);

            //   fileTransfer.download(
            //       uri,
            //       target,
            //       function(entry) {
            //           console.log("download complete: " + entry.toURL());
            //       },
            //       function(error) {
            //           console.log("download error source " + error.source);
            //           console.log("download error target " + error.target);
            //           console.log("upload error code" + error.code);
            //       },
            //       false
            //   );
            // }

            var uriString = "http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_5mb.mp4";
            var filename = uriString.split("/").pop();


            var moveFile = function(fileEntry) {

                console.log('moving file...');

                window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (dirEntry) {

                    console.log('file system open: ' + dirEntry.name);

                    fileEntry.moveTo(dirEntry, 'wtf.mp4', function(fileEntry) {
                        console.log('succesfully moved file: status - %0', fileEntry);

                        $scope.download.fileEntry = fileEntry;

                    }, function() {
                        console.log('failed moved file');
                        console.log(arguments);
                    });
                });
            };

            //TODO: External is Anroid only - need to handle IOS  
            //resolveLocalFileSystemURL: Retrieve a DirectoryEntry or FileEntry using local URL. (Function)  
            window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function(dirEntry) {

                console.log('directory entry: %0', dirEntry);

                dirEntry.getFile(filename, { create: true }, function(fileEntry) {

                    console.log('target file: %0', fileEntry);

                    var onSuccess = function(status) {
                        console.log('download success - status: ', status);

                        $scope.download.bytesReceived = $scope.download.bytesTotal;
                        $scope.$apply();

                        moveFile(fileEntry);
                    };

                    var onError = function() {
                        console.log('download error: %0', arguments);
                        console.log(arguments);
                    };

                    var onProgress = function(data) {
                        console.count();
                        //console.log(data);

                        $scope.download.bytesReceived = data.bytesReceived;

                        if(!$scope.download.bytesTotal) {
                            $scope.download.bytesTotal = data.totalBytesToReceive;
                        }

                        $scope.$apply();

                    };

                    var downloader = new BackgroundTransfer.BackgroundDownloader();

                    // Create a new download operation.
                    var download = downloader.createDownload(uriString, fileEntry);
                    // Start the download and persist the promise to be able to cancel the download.
                    var downloadPromise = download.startAsync().then(onSuccess, onError, onProgress);



                }, function(e) {
                    console.log('get file error');
                    console.log(e);
                })

            }, function(e) {
                console.log('resolve error');
                console.log(e);
            });


        };

        $scope.playVideo = function () {
            console.log('play video');
             // Play a video with callbacks
              var options = {
                successCallback: function() {
                  console.log("Video was closed without error.");
                },
                errorCallback: function(errMsg) {
                  console.log("Error! " + errMsg);
                },
                orientation: 'landscape'
              };
              window.plugins.streamingMedia.playVideo($scope.download.fileEntry.nativeURL, options);

        };





})
.controller('FeedCtrl', function($scope, FeedService) {

    var items = [];

    FeedService.getFeed().then(function successCallback(response) {

        $scope.items = response;

    }, function errorCallback(response) {

    });    




})
.controller('ChatsCtrl', function($scope, Chats) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    $scope.chats = Chats.all();

    $scope.remove = function(chat) {
        Chats.remove(chat);
    };


})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
    console.log(arguments);
    $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
    $scope.settings = {
        enableFriends: true
    };
});
