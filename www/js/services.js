angular.module('starter.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }


  ];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
})

.factory('FeedService', function ($http, $q){

    this.getFeed = function() {   

        return $http.get('http://localhost:5000/feed/').then(function(response) {

              return response.data;
        });         
    };

    return this;
})

.service('DBService', function($q) {
  var items;
  var db;
  var self = this;

  this.initDB = function() {
    return db = new PouchDB('simpleDB', {
      adapter: 'websql',
      iosDatabaseLocation: 'default'
    });
  };

  this.getDB = function() {

    if (!items) {
      return $q.when(
          db.allDocs({
            include_docs: true
          }))
        .then(function(docs) {
          items = docs.rows.map(function(row) {
            row.doc.Date = new Date(row.doc.Date);
            return row.doc;
          });

          // Listen for changes on the database.
          db.changes({
              live: true,
              since: 'now',
              include_docs: true
            })
            .on('change', function(change) {
              self.onDatabaseChange(change)
            });
          return items;

        });
    } else {
      return $q.when();
    };

  };

  this.onDatabaseChange = function(change) {
    var index = self.findIndex(items, change.id);
    var item = items[index];

    items.splice(index, 0, change.doc) // insert
  };

  this.findIndex = function(array, id) {
    var low = 0,
      high = array.length,
      mid;
    while (low < high) {
      mid = (low + high) >>> 1;
      array[mid]._id < id ? low = mid + 1 : high = mid
    }
    return low;
  };

  this.storeData = function(data) {
    return $q.when(db.post({
      'title': data
    }))
  };

  this.destroy = function () {
    console.log(db);
    db.destroy().then(function (response) {
       console.log('db destroyed');
    }).catch(function (err) {
      console.log(err);
    });
  };

  this.getInfo = function () {
    //This will print some database information, including the attribute sqlite_plugin, which will be true if the SQLite Plugin is being used.
    db.info().then(console.log.bind(console));
  };

  return this
});
